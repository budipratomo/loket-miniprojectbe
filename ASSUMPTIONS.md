# Development Environtment
* PHP 7.2.12
* MariaDb 10.1.21
* Lumen (Laravel's micro framework to build a Api web service)


# Assumtions
* Lumen installer already installed on host server
* Lumen requirement satisfied (https://lumen.laravel.com/docs/5.7#installation)
* Each table has _created\_at_ and _updated\_at_ field to store creating and updating time

* No status event (like draft and publish), simple event creator
* Customer can buy many tickects of one event without register every ticket owner's data (general ticket)
* No authentication
* One event minimum has 1 category and maximum has 3 categories
* API specification can be found at this site (TODO: make api specification)
* By default database already has categories data and location data.
* Relation between Event and Schedule is one-one
* Location can be used one or more event, when create event if location value is integer then system will use existing location based on the location id, otherwise if location value is object then system will create new location and assigned the id to the event.
* Api endpoint /event/get_info is to get specific event information, id is required
* Event quota based on tickets quota
* All response field for exam purpose, if going to production environment should protect some fields
* API endpoints follow the rules of the exam's question, ignore API endpoint best practice implementation (example: /events/1/info)

# Installation
* Extract archived files included or clone from repository  (https://gitlab.com/budipratomo/loket-miniprojectbe.git)
* Run composer install in extracted directory, make sure all package requirement installed
* Duplicate .env.example to `.env` file and set database variables based on your testing environment
* You can manually deploy database with `.sql` file included or you can run artisan command `php artisan migrate --seed` to create database schema.
* Run server with `php -S localhost:8000 -t public` (from project directory)
* JSON Api Service good to go.


# Response format

Success:
```
Http status code: 200
	
{
	status: true,
	error: null,
	result: [json data]
}
```

Fail:
```
Http status code: 200

{
	status: false,
	result: null
	error: {
		code: 1087,
		message: "Error message"
	}
}
```