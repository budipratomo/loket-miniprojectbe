<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 03 Jan 2019 07:26:30 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Event
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $banner
 * @property int $location_id
 * @property int $schedule_id
 * @property int $organizer_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property Location $location
 * @property Organizer $organizer
 * @property Schedule $schedule
 * @property \Illuminate\Database\Eloquent\Collection $event_categories
 * @property \Illuminate\Database\Eloquent\Collection $orders
 * @property \Illuminate\Database\Eloquent\Collection $tickets
 *
 * @package App\Models
 */
class Event extends Eloquent
{
    protected $casts = [
        'location_id' => 'int',
    ];

    protected $fillable = [
        'title',
        'description',
        'banner',
        'organizer',
        'location_id',
    ];

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function schedule()
    {
        return $this->hasOne(Schedule::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'event_categories', 'event_id', 'category_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
}
