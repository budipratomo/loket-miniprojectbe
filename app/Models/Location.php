<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 03 Jan 2019 07:26:30 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Location
 * 
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $city
 * @property int $latitude
 * @property int $longitude
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $events
 *
 * @package App\Models
 */
class Location extends Eloquent
{

	protected $fillable = [
		'name',
		'address',
		'city',
		'latitude',
		'longitude',
	];

	public function events()
	{
		return $this->hasMany(Event::class);
	}
}
