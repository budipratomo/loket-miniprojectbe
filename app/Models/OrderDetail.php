<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;


class OrderDetail extends Eloquent
{
	protected $casts = [
		'ticket_id' => 'int',
		'order_id' => 'int'
	];

	protected $fillable = [
		'ticket_number',
		'ticket_id',
		'order_id',
	];

	public function order()
	{
		return $this->belongsTo(Order::class);
	}

	public function ticket()
	{
		return $this->belongsTo(Ticket::class);
	}
}
