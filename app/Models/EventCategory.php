<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 03 Jan 2019 07:26:30 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class EventCategory
 * 
 * @property int $id
 * @property int $category_id
 * @property int $event_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property Category $category
 * @property Event $event
 *
 * @package App\Models
 */
class EventCategory extends Eloquent
{
	protected $casts = [
		'category_id' => 'int',
		'event_id' => 'int'
	];

	protected $fillable = [
		'category_id',
		'event_id'
	];

	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	public function event()
	{
		return $this->belongsTo(Event::class);
	}
}
