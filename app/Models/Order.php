<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 03 Jan 2019 07:26:30 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Order
 *
 * @property int $id
 * @property string $order_number
 * @property int $event_id
 * @property int $customer_id
 * @property string $additional_info
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property Customer $customer
 * @property Event $event
 *
 * @package App\Models
 */
class Order extends Eloquent
{
    protected $casts = [
        'customer_id' => 'int'
    ];

    protected $fillable = [
        'customer_id',
        'additional_info'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function details()
    {
        return $this->hasMany(OrderDetail::class);
    }
}
