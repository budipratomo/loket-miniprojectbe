<?php
/**
 * Created by PhpStorm.
 * User: Budi Pratomo
 * Date: 03-Jan-19
 * Time: 1:05 PM
 */

namespace App\Models;


/**
 * Class ErrorMessage
 * @package App\Models
 */
class ErrorMessage
{

    /**
     * Error code
     *
     * @var string
     */
    public $code;

    /**
     * Error message
     *
     * @var string
     */
    public $message;

    /**
     * ErrorMessage constructor.
     *
     * @param string $code
     * @param string $message
     */
    public function __construct(string $code, string $message)
    {
        $this->code = $code;
        $this->message = $message;
    }


}