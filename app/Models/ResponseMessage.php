<?php

namespace App\Models;

/**
 * Class ResponseMessage
 * @package App\Models
 */
class ResponseMessage
{
    /**
     * Response status
     * @var bool
     */
    public $status;

    /**
     * Error message
     *
     * @var ErrorMessage
     */
    public $error;

    /**
     * Request result (if success)
     *
     * @var mixed
     */
    public $result;

    /**
     * ResponseMessage constructor.
     *
     * @param $status
     * @param $result
     * @param null $error
     */
    public function __construct($status, $result, $error = null)
    {
        $this->status = $status;
        $this->result = $result;
        $this->error = $error;
    }

    /**
     * Make success response message
     *
     * @param $result
     * @return ResponseMessage
     */
    public static function makeResult($result)
    {
        return new ResponseMessage(true, $result);
    }

    /**
     * Make error response message
     *
     * @param $code
     * @param $message
     * @return ResponseMessage
     */
    public static function makeError($code, $message)
    {
        return new ResponseMessage(false, null, new ErrorMessage($code, $message));
    }



}