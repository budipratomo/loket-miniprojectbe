<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 03 Jan 2019 07:26:30 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Ticket
 *
 * @property int $id
 * @property int $event_id
 * @property string $name
 * @property int $quota
 * @property int $left_quota
 * @property int $price
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property Event $event
 *
 * @package App\Models
 */
class Ticket extends Eloquent
{
    protected $casts = [
        'event_id' => 'int',
        'quota' => 'int',
        'price' => 'int'
    ];

    protected $fillable = [
        'event_id',
        'name',
        'quota',
        'price',
        'description'
    ];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
