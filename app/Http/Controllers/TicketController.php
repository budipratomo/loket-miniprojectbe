<?php

namespace App\Http\Controllers;

use App\Models\ResponseMessage;
use App\Models\Ticket;
use \Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TicketController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function create(Request $request)
    {
        try {
            //Validate required data
            $this->validate($request, [
                'event_id' => 'required|exists:events,id',
                'name' => 'required|max:255',
                'quota' => 'required|integer',
                'price' => 'required|numeric',
            ]);

            //Create new ticket
            $ticket = new Ticket();
            $ticket->fill($request->all());
            $ticket->left_quota = $ticket->quota;
            $ticket->save();

            //Return id of the inserted location
            return response()->json(ResponseMessage::makeResult($ticket->id));
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(ResponseMessage::makeError($e->getCode(), $e->getMessage()));
        }
    }

}
