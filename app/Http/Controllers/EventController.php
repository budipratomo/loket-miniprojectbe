<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Location;
use App\Models\ResponseMessage;
use App\Models\Schedule;
use \Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function create(Request $request)
    {
        try {
            //Validate required data
            $this->validate($request, [
                'title' => 'required|max:255',
                'categories' => 'required|array|between:1,3',
                'categories.*' => 'distinct|exists:categories,id',
                'organizer' => 'required|max:255',
                'location' => 'required',
                'schedule' => 'required',
                'schedule.start_date' => 'required|date',
                'schedule.end_date' => 'required|date',
                'schedule.start_time' => 'required',
                'schedule.end_time' => 'required',

            ]);

            DB::beginTransaction();

            //Create new event
            $event = new Event();
            $event->fill($request->all());

            //Location
            if (is_int($request->input('location'))) {
                $location = Location::where('id', $request->input('location'))->first();
            } else {
                //Create new location
                $location = new Location();
                $location->fill($request->input('location'));
                $location->save();
            }
            //Associate location with event
            if ($location == null) throw new Exception("Location not registered", 500);
            $event->location()->associate($location);
            $event->save();

            //Create Schedule
            $event->schedule()->create($request->input('schedule'));

            //Attach categories to this event
            $event->categories()->attach($request->input('categories'));

            DB::commit();

            //Return id of the inserted event
            return response()->json(ResponseMessage::makeResult($event->id));
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(ResponseMessage::makeError($e->getCode(), $e->getMessage()));
        }
    }

    public function getInfo(Request $request)
    {
        try {
            $eventId = $request->get('id');

            //Get specific event info
            $event = Event::where('id', $eventId)->first();
            if ($event == null) {
                //return null when event not found
                $result = null;
            } else {
                //Build result and detail event info
                $result = $event->toArray();
                $result['schedule'] = $event->schedule;
                $result['tickets'] = $event->tickets;
                $result['location'] = $event->location;
            }

            return response()->json(ResponseMessage::makeResult($result));
        } catch (Exception $e) {
            return response()->json(ResponseMessage::makeError($e->getCode(), $e->getMessage()));
        }
    }
}
