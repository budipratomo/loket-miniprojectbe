<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Order;
use App\Models\ResponseMessage;
use App\Models\Ticket;
use \Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function purchase(Request $request)
    {
        try {
            //Validate required data
            $this->validate($request, [
                'tickets' => 'required',
                'tickets.*.id' => 'required|exists:tickets,id',
                'tickets.*.qty' => 'required|integer',
                'customer' => 'required',
                'customer.name' => 'required|max:255',
                'customer.email' => 'required|email',
                'customer.phone' => 'required|max:255',
            ]);

            DB::beginTransaction();

            $customer = new Customer();
            $customer->fill($request->get('customer'));
            $customer->save();

            //Create transaction order
            $order = new Order();
            $order->order_number = Str::random(30); //sample of order number generator
            $customer->orders()->save($order);

            //Check quota of the ticket
            //If one of the buying ticket not satisfied then cancel all, rollback transaction
            $buyTickets = $request->get('tickets');
            foreach ($buyTickets as $buyTicket) {
                $ticket = Ticket::where('id', $buyTicket['id'])->firstOrFail();
                if ($ticket->left_quota < $buyTicket['qty']) throw new Exception("Ticket quota exceed");

                //Quota satisfied, generate ticket for customer (order detail)
                for ($i = 0; $i < $buyTicket['qty']; $i++) {
                    $order->details()->create([
                        'ticket_number' => Str::random(30), //sample of ticket number generator
                        'ticket_id' => $ticket->id,
                    ]);
                }

                $ticket->left_quota -= $buyTicket['qty'];
                $ticket->save();
            }

            DB::commit();

            //Return id of the inserted transaction
            return response()->json(ResponseMessage::makeResult($order->id));
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(ResponseMessage::makeError($e->getCode(), $e->getMessage()));
        }
    }

    public function getInfo(Request $request)
    {
        try {
            $orderId = $request->get('id');

            //Get transaction info
            $order = Order::where('id', $orderId)->first();
            if ($order == null) {
                //return null when transaction not found
                $result = null;
            } else {
                //Build result and detail transaction info
                $result = $order->toArray();
                $result['detail'] = $order->details;
            }

            return response()->json(ResponseMessage::makeResult($result));
        } catch (Exception $e) {
            return response()->json(ResponseMessage::makeError($e->getCode(), $e->getMessage()));
        }
    }
}
