<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\ResponseMessage;
use \Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function create(Request $request)
    {
        try {
            //Validate required data
            $this->validate($request, [
                'name' => 'required|max:255',
                'address' => 'required|max:255',
                'city' => 'required|max:255',
                'latitude' => 'numeric',
                'longitude' => 'numeric',

            ]);

            //Create new location
            $location = new Location();
            $location->fill($request->all());
            $location->save();

            //Return id of the inserted location
            return response()->json(ResponseMessage::makeResult($location->id));
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(ResponseMessage::makeError($e->getCode(), $e->getMessage()));
        }
    }

}
