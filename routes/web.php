<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group( ['prefix' => 'event'], function () use ($router) {
    // /event/create
    $router->post('create', 'EventController@create');
    // /event/get_info
    $router->get('get_info', 'EventController@getInfo');
});


$router->group( ['prefix' => 'location'], function () use ($router) {
    // /location/create
    $router->post('create', 'LocationController@create');
});


$router->group( ['prefix' => 'ticket'], function () use ($router) {
    // /ticket/create
    $router->post('create', 'TicketController@create');
});


$router->group( ['prefix' => 'transaction'], function () use ($router) {
    // /transaction/purchase
    $router->post('purchase', 'TransactionController@purchase');
    // /transaction/get_info
    $router->get('get_info', 'TransactionController@getInfo');
});