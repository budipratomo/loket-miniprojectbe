<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Categories Seeder
        DB::table('categories')->insert(['name' => 'Festival Electronic Dance Music']);
        DB::table('categories')->insert(['name' => 'Festival General']);
        DB::table('categories')->insert(['name' => 'Concert']);
        DB::table('categories')->insert(['name' => 'Sports']);
        DB::table('categories')->insert(['name' => 'Exhibition']);
        DB::table('categories')->insert(['name' => 'Conference']);
        DB::table('categories')->insert(['name' => 'Workshop']);
        DB::table('categories')->insert(['name' => 'Theater & Drama Musical']);
        DB::table('categories')->insert(['name' => 'Attraction']);
        DB::table('categories')->insert(['name' => 'Accommodation']);
        DB::table('categories')->insert(['name' => 'Transportation']);
        DB::table('categories')->insert(['name' => 'Other']);

        //Locations
        DB::table('locations')->insert([
            'name'=> 'Balai Sarbini Jakarta',
            'address'=> 'Jl. Jend. Sudirman No.Kav. 50, RT.1/RW.4, Karet Semanggi, Kecamatan Setiabudi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12930',
            'city'=> 'Jakarta'
        ]);



    }
}
