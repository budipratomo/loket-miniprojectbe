<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventCategoriesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'event_categories';

    /**
     * Run the migrations.
     * @table event_categories
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('event_id');
            $table->timestamps();

            $table->index(["category_id"], 'fk_event_category');

            $table->index(["event_id"], 'fk_event_event');


            $table->foreign('category_id', 'fk_event_category')
                ->references('id')->on('categories')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreign('event_id', 'fk_event_event')
                ->references('id')->on('events')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
