<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'order_details';

    /**
     * Run the migrations.
     * @table orders
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('ticket_number');
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('ticket_id');
            $table->timestamps();

            $table->index(["order_id"], 'fk_order_detail');

            $table->index(["ticket_id"], 'fk_order_ticket');

            $table->foreign('order_id', 'fk_order_detail')
                ->references('id')->on('orders')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('ticket_id', 'fk_order_ticket')
                ->references('id')->on('tickets')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
